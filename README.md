# ESP8266 IoT Data Logger Init Script

Init script for esp8266 IoT Data Logger board  http://pcb.daince.net/doku.php?id=esp8266_iot_dl_v100

## Librray for Arduino IDE ##
*  [esp8266 Board library](https://github.com/esp8266/Arduino)
*  [DS3231 RTC library](https://github.com/Makuna/Rtc/wiki)
*  [DS18B20 library](https://github.com/milesburton/Arduino-Temperature-Control-Library)